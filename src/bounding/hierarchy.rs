use rand::prelude::*;

use crate::raytracer::vector3::Vector3;
use crate::raytracer::ray::Ray;
use crate::objects::triangle::Triangle;

use super::common::*;
use super::aabb::AABB;

#[derive(Clone, Debug)]
pub enum HierarchyChildren {
	H(Vec<Hierarchy>),
	T(Vec<Triangle>),
}

#[derive(Clone, Debug)]
pub struct Hierarchy {
	pub number_of_triangles: usize,
	pub bounding_volume: AABB,
	pub children: HierarchyChildren,
	pub color: Vector3,
	pub test_triangle: Triangle
}

impl Hierarchy {
	fn make_bounding_volume(min: Vector3, max: Vector3) -> AABB {
		return AABB::new((min, max));
	}

	pub fn new(triangles: &mut Vec<Triangle>) -> Hierarchy {
		triangles.sort_by(compare_triangles_x);

		let bb: (Vector3, Vector3);
		if triangles.iter().count() > 0 {
			bb = bounds(&triangles);
		} else {
			bb = (Vector3::new(0.0, 0.0, 0.0), Vector3::new(0.0, 0.0, 0.0));
		}

		let mut a: Vec<Triangle> = triangles[..(triangles.iter().count() / 2)].to_vec();
		let mut b: Vec<Triangle> = triangles[(triangles.iter().count() / 2)..].to_vec();

		if triangles.iter().count() > 1000 {
			return Hierarchy {
				number_of_triangles: triangles.iter().count(),
				bounding_volume: Hierarchy::make_bounding_volume(bb.0, bb.1),
				children: HierarchyChildren::H(vec!(
					Hierarchy::new(&mut a),
					Hierarchy::new(&mut b)
				)),
				color: Vector3::new(0.0, 0.0, 0.0),
				test_triangle: Triangle::new(
					Vector3::new(0.0, 0.0, 0.0),
					Vector3::new(0.0, 0.0, 0.0),
					Vector3::new(0.0, 0.0, 0.0),
					Vector3::new(0.0, 0.0, 0.0)
				)
			};
		} else if triangles.iter().count() > 1 {
			return Hierarchy {
				number_of_triangles: triangles.iter().count(),
				bounding_volume: Hierarchy::make_bounding_volume(bb.0, bb.1),
				children: HierarchyChildren::H(vec!(
					Hierarchy {
						number_of_triangles: a.iter().count(),
						bounding_volume: Hierarchy::make_bounding_volume(bounds(&a).0, bounds(&a).1),
						children: HierarchyChildren::T(a),
						color: Vector3::new(0.5, 0.0, 0.0),
						test_triangle: Triangle::new(
							Vector3::new(0.0, 0.0, 0.0),
							Vector3::new(0.0, 0.0, 0.0),
							Vector3::new(0.0, 0.0, 0.0),
							Vector3::new(rand::random(), rand::random(), rand::random())
						)
					},
					Hierarchy {
						number_of_triangles: b.iter().count(),
						bounding_volume: Hierarchy::make_bounding_volume(bounds(&b).0, bounds(&b).1),
						children: HierarchyChildren::T(b),
						color: Vector3::new(0.5, 0.0, 0.0),
						test_triangle: Triangle::new(
							Vector3::new(0.0, 0.0, 0.0),
							Vector3::new(0.0, 0.0, 0.0),
							Vector3::new(0.0, 0.0, 0.0),
							Vector3::new(rand::random(), rand::random(), rand::random())
						)
					}
				)),
				color: Vector3::new(0.5, 0.0, 0.0),
				test_triangle: Triangle::new(
					Vector3::new(0.0, 0.0, 0.0),
					Vector3::new(0.0, 0.0, 0.0),
					Vector3::new(0.0, 0.0, 0.0),
					Vector3::new(rand::random(), rand::random(), rand::random())
				)
			};
		} else {
			// Only one triangle.
			return Hierarchy {
				number_of_triangles: triangles.iter().count(),
				bounding_volume: Hierarchy::make_bounding_volume(bb.0, bb.1),
				children: HierarchyChildren::T(triangles.clone()),
				color: Vector3::new(0.0, 0.0, 0.5),
				test_triangle: Triangle::new(
					Vector3::new(0.0, 0.0, 0.0),
					Vector3::new(0.0, 0.0, 0.0),
					Vector3::new(0.0, 0.0, 0.0),
					Vector3::new(rand::random(), rand::random(), rand::random())
				)
			};
		}
	}

	fn intersect_children(&self, ray: Ray) -> Option<(f32, &Triangle)> {
		let mut child_intersections: Vec<(f32, &Triangle)> = match &self.children {
			HierarchyChildren::H(c) => {
				c.iter()
					.filter_map(|c| c.intersect(ray))
					.collect()
			}

			HierarchyChildren::T(c) => {
				c.iter()
					.filter_map(|c| c.intersect(ray))
					.collect()
			}
		};

		if child_intersections.iter().count() > 0 {
			child_intersections.sort_by(|a, b| a.0.partial_cmp(&b.0).unwrap());
			return Some(*child_intersections.first().unwrap());
		} else {
			return None
		}
	}

	pub fn intersect(&self, ray: Ray) -> Option<(f32, &Triangle)> {
		let intersects_me: Option<f32> = self.bounding_volume.intersect(ray);
		match intersects_me {
			Some(t) => {
				return self.intersect_children(ray)
			},
			None => return None
		}
	}
}
