use crate::raytracer::vector3::Vector3;
use crate::objects::triangle::Triangle;

use std::cmp::Ordering;

pub fn greatest_value_on_axis(triangle: &Triangle, axis: &str) -> f32 {
	let one: f32;
	let two: f32;
	let three: f32;
	match axis {
		"x" => {
			one = triangle.v0.x;
			two = triangle.v1.x;
			three = triangle.v2.x;
		},
		"y" => {
			one = triangle.v0.y;
			two = triangle.v1.y;
			three = triangle.v2.y;
		},
		"z" => {
			one = triangle.v0.z;
			two = triangle.v1.z;
			three = triangle.v2.z;
		}
		_ => {
			one = triangle.v0.x;
			two = triangle.v1.x;
			three = triangle.v2.x;
		}
	}

	return one.max(two.max(three));
}

pub fn compare_triangles_x(a: &Triangle, b: &Triangle) -> Ordering {
	if greatest_value_on_axis(a, "x") > greatest_value_on_axis(b, "x") {
		return Ordering::Greater;
	} else {
		return Ordering::Less;
	}
}

pub fn greatest_and_least_on_axis(triangles: &Vec<Triangle>, axis: &str) -> (f32, f32) {
	let mut axis_values: Vec<f32>;
	match axis {
		"y" => {
			let vertices: Vec<Vec<Vector3>> = triangles.iter().map(|t| vec![t.v0, t.v1, t.v2]).collect();
			let values: Vec<f32> = vertices.iter().flatten().map(|v| v.y).collect();
			axis_values = values;
		},
		"z" => {
			let vertices: Vec<Vec<Vector3>> = triangles.iter().map(|t| vec![t.v0, t.v1, t.v2]).collect();
			let values: Vec<f32> = vertices.iter().flatten().map(|v| v.z).collect();
			axis_values = values;
		},
		"x" => {
			let vertices: Vec<Vec<Vector3>> = triangles.iter().map(|t| vec![t.v0, t.v1, t.v2]).collect();
			let values: Vec<f32> = vertices.iter().flatten().map(|v| v.x).collect();
			axis_values = values;
		},
		_ => {
			let vertices: Vec<Vec<Vector3>> = triangles.iter().map(|t| vec![t.v0, t.v1, t.v2]).collect();
			let values: Vec<f32> = vertices.iter().flatten().map(|v| v.x).collect();
			axis_values = values;
		}
	}

	axis_values.sort_by(|a, b| a.partial_cmp(b).expect("Float comparison failed."));
	return (axis_values.first().expect("Vector is empty.1").clone(), axis_values.last().expect("Vector is empty.2").clone());
}

pub fn bounds(triangles: &Vec<Triangle>) -> (Vector3, Vector3){
	let bb_x: (f32, f32) = greatest_and_least_on_axis(triangles, "x");
	let bb_y: (f32, f32) = greatest_and_least_on_axis(triangles, "y");
	let bb_z: (f32, f32) = greatest_and_least_on_axis(triangles, "z");

	return (Vector3::new(bb_x.0, bb_y.0, bb_z.0), Vector3::new(bb_x.1, bb_y.1, bb_z.1));
}
