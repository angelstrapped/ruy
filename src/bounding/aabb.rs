use crate::raytracer::vector3::Vector3;
use crate::raytracer::ray::Ray;

#[derive(Clone, Debug)]
pub struct AABB {
	pub bounding_box: (Vector3, Vector3)
}

impl AABB {
	pub fn new(bounding_box: (Vector3, Vector3)) -> AABB {
		AABB {
			bounding_box: bounding_box
		}
	}

	pub fn intersect(&self, ray: Ray) -> Option<f32> {
		// Inverse is supposed to be faster.
		let inverse_x: f32 = 1.0 / ray.direction.x;

		let mut tx: Vec<f32> = vec!(
			((self.bounding_box.0.x - ray.origin.x) * inverse_x),
			((self.bounding_box.1.x - ray.origin.x) * inverse_x)
		);

		tx.sort_by(|a, b| a.partial_cmp(&b).unwrap());

		let inverse_y: f32 = 1.0 / ray.direction.y;

		let mut ty: Vec<f32> = vec!(
			((self.bounding_box.0.y - ray.origin.y) * inverse_y),
			((self.bounding_box.1.y - ray.origin.y) * inverse_y)
		);

		ty.sort_by(|a, b| a.partial_cmp(&b).unwrap());

		let inverse_z: f32 = 1.0 / ray.direction.z;

		let mut tz: Vec<f32> = vec!(
			((self.bounding_box.0.z - ray.origin.z) * inverse_z),
			((self.bounding_box.1.z - ray.origin.z) * inverse_z)
		);

		tz.sort_by(|a, b| a.partial_cmp(&b).unwrap());

		let tmin = tx[0].max(ty[0]).max(tz[0]);
		let tmax = tx[1].min(ty[1]).min(tz[1]);

		if tmax >= tmin {
			return Some(tmin);
		} else {
			return None;
		}
	}
}
