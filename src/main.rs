extern crate clap;

mod raytracer;
mod drivers;
mod objects;
mod island;
mod bounding;

use std::path::Path;
use std::time::SystemTime;

use clap::{Arg, App, SubCommand};

use raytracer::raytracer::Raytracer;
use raytracer::vector3::Vector3;

use drivers::dae::DAE;
use drivers::image::Image;

use objects::scene::Scene;

use island::island::Island;

// Width and height must be usize to create the 2D pixel array.
// Anything that is used as an index, apparently, needs to be usize.
// Makes sense, really.
const WIDTH: usize = 128;
const HEIGHT: usize = 72;
const COLOR_DEPTH: u32 = 256;
const NUMBER_OF_PASSES: usize = 2;
const DEFAULT_RANDOM_MULTIPLIER: f32 = 0.0;
const DEFAULT_NUM_RANDOM_SAMPLES: usize = 1;
const DEFAULT_NUM_DIFFUSE_SAMPLES: usize = 1;

fn main() {
	let matches = App::new("Ruy")
		.version("0.0.1")
		.author("tokc <eviloatmeal@eviloatmeal.com>")
		.about("Somewhat of a raytracer.")
		.arg(Arg::with_name("scene")
			.short("s")
			.long("scene")
			.value_name("FILE")
			.help("Load a .dae file as the scene.")
			.required(false)
			.takes_value(true)
		)
		.arg(Arg::with_name("filename")
			.short("o")
			.long("output")
			.value_name("FILE")
			.help("Output to this file. Default is <timestamp>.jpg.")
			.required(false)
			.takes_value(true)
		)
		.arg(Arg::with_name("width")
			.index(1)
			.help("Image width.")
			.required(false)
			.takes_value(true)
		)
		.arg(Arg::with_name("height")
			.index(2)
			.help("Image height.")
			.required(false)
			.takes_value(true)
		)
		.arg(Arg::with_name("random")
			.long("rpass")
			.value_name("DECIMAL NUMBER")
			.help("Strength of random pass in the final image. 0.0 to 1.0. 0.1 - 0.2 is probably a good value.")
			.required(false)
			.takes_value(true)
		)
		.arg(Arg::with_name("samples")
			.long("samples")
			.value_name("NUMBER")
			.help("Average number of random samples per pixel.")
			.required(false)
			.takes_value(true)
		)
		.arg(Arg::with_name("dsamples")
			.long("dsamples")
			.value_name("NUMBER")
			.help("Number of diffuse rays to trace per pixel.")
			.required(false)
			.takes_value(true)
	)
		.subcommand(SubCommand::with_name("testset")
			.about("Render a set of images to test changes.")
			.arg(Arg::with_name("num_images")
				.index(1)
				.help("Number of images to render.")
				.required(true)
				.takes_value(true)
			)
			.arg(Arg::with_name("width")
				.index(2)
				.help("Image width.")
				.required(true)
				.takes_value(true)
			)
			.arg(Arg::with_name("height")
				.index(3)
				.help("Image height.")
				.required(true)
				.takes_value(true)
			)
		)
		.get_matches();

	let samples: usize = match matches.value_of("samples") {
		Some(w) => w.parse::<usize>().unwrap(),
		None => DEFAULT_NUM_RANDOM_SAMPLES
	};

	let random: f32 = match matches.value_of("random") {
		Some(w) => w.parse::<f32>().unwrap(),
		None => DEFAULT_RANDOM_MULTIPLIER
	};

	let num_diffuse_samples: usize = match matches.value_of("dsamples") {
		Some(w) => w.parse::<usize>().unwrap(),
		None => DEFAULT_NUM_DIFFUSE_SAMPLES
	};

	let filename: String;
	match matches.value_of("filename") {
		Some(f) => filename = f.to_string(),
		None => {
			filename = SystemTime::now()
				.duration_since(SystemTime::UNIX_EPOCH)
				.expect("No time.")
				.as_secs()
				.to_string() + ".jpg";
		}
	}

	let scene: Scene;
	match matches.value_of("scene") {
		Some(s) => {
			let path: &Path = Path::new(s);
			scene = DAE::load_scene(path);
		},
		None => {
			scene = Island::generate_scene();
		}
	}

	let test = matches.subcommand_matches("testset");
	match test {
		Some(t) => {
			let iterations: usize = t.value_of("num_images")
				.unwrap_or_else(|| "1")
				.parse::<usize>()
				.unwrap();

			let width: usize = t.value_of("width")
				.unwrap_or_else(|| "10")
				.parse::<usize>()
				.unwrap();

			let height: usize = t.value_of("height")
				.unwrap_or_else(|| "10")
				.parse::<usize>()
				.unwrap();

			println!("{} {}", width, height);

			for i in 0..iterations {
				render(Island::generate_scene(), width, height, 256, format!("{}.jpg", i), samples, num_diffuse_samples, random);
			}
		}
		None => {
			let x: usize = match matches.value_of("width") {
				Some(w) => w.parse::<usize>().unwrap(),
				None => WIDTH
			};

			let y: usize = match matches.value_of("height") {
				Some(h) => h.parse::<usize>().unwrap(),
				None => HEIGHT
			};

			render(scene, x, y, COLOR_DEPTH, filename.to_string(), samples, num_diffuse_samples, random);
		}
	}
}

fn render(scene: Scene, width: usize, height: usize, color_depth: u32, filename: String, samples: usize, num_diffuse_samples: usize, random: f32) {
	let mut image = Image::new(width, height, color_depth, Vector3::new(0.0, 0.0, 0.0));
	let raytracer: Raytracer = Raytracer::new(scene);
	let pass_multiplier: f32;;
	if random > 0.01 {
		pass_multiplier = random;
	} else {
		pass_multiplier = 0.0;
	}

	raytracer.render(&mut image, 1.0 - pass_multiplier, samples, num_diffuse_samples);
	
	if random > 0.01 {
		raytracer.render_random(&mut image, pass_multiplier, samples, num_diffuse_samples);
	}

	image.save_jpg(Path::new(&filename));
}
