use super::camera::Camera;
use super::light::PointLight;
use super::triangle::Triangle;

use crate::bounding::hierarchy::Hierarchy;

#[derive(Clone, Debug)]
pub struct Scene {
    pub objects: Vec<Hierarchy>,
    pub lights: Vec<PointLight>,
    pub active_camera: Camera
}

impl Scene {
    pub fn new(objects: Vec<Hierarchy>, lights: Vec<PointLight>, active_camera: Camera) -> Scene {
        return Scene {
            objects: objects,
            lights: lights,
            active_camera: active_camera
        }
    }
}
