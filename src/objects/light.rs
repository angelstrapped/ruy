use crate::raytracer::vector3::Vector3;

#[derive(Copy, Clone, Debug)]
pub struct PointLight {
    pub origin: Vector3,
    pub direction: Vector3,
    pub color: Vector3,
    pub brightness: f32
}

impl PointLight {
    pub fn new(origin: Vector3, direction: Vector3, color: Vector3, brightness: f32) -> PointLight {
        return PointLight {
            origin: origin,
            direction: direction,
            color: color,
            brightness: brightness
        }
    }
}
