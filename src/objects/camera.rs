use crate::raytracer::vector3::Vector3;

#[derive(Copy, Clone, Debug)]
pub struct Camera {
    pub origin: Vector3,
    pub direction: Vector3,
    pub focal_length: f32
}

impl Camera {
    pub fn new(origin: Vector3, direction: Vector3, focal_length: f32) -> Camera {
        return Camera {
            origin: origin,
            direction: direction,
            focal_length: focal_length
        }
    }
}
