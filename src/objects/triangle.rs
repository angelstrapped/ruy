use crate::raytracer::vector3::Vector3;
use crate::raytracer::ray::Ray;

static EPSILON: f32 = 0.0001;

#[derive(Copy, Clone, Debug)]
pub struct Triangle {
    pub v0: Vector3,
    pub v1: Vector3,
    pub v2: Vector3,

    e1: Vector3,
    e2: Vector3,

    pub n: Vector3,

    pub color: Vector3
}

impl Triangle {
    pub fn new(
        v0: Vector3,
        v1: Vector3,
        v2: Vector3,
        color: Vector3
    ) -> Triangle {
        let e1: Vector3 = v1 - v0;
        let e2: Vector3 = v2 - v0;
        let n: Vector3 = e1.cross(e2);

        return Triangle {
            v0: v0,
            v1: v1,
            v2: v2,
            e1: e1,
            e2: e2,
            n: n,
            color: color
        }
    }

    pub fn normal(&self) -> Vector3 {
        return self.n;
    }

    pub fn intersect(&self, ray: Ray) -> Option<(f32, &Triangle)> {
        // Here's Möller-Trumbore, again.
        let h: Vector3 = ray.direction.cross(self.e2);
        let a: f32 = self.e1.dot(h);
        if a.abs() < EPSILON {
            // Basically parallel, no hit.
            return None;
        }

        let f: f32 = 1.0 / a;
        let s: Vector3 = ray.origin - self.v0;
        let u: f32 = f * s.dot(h);
        if (u < 0.0) || (u > 1.0) {
            return None;
        }

        let q: Vector3 = s.cross(self.e1);
        let v: f32 = f * ray.direction.dot(q);
        if (v < 0.0) || (u + v > 1.0) {
			return None;
        }

        let t: f32 = f * self.e2.dot(q);
        if t > EPSILON {
            return Some((t, &self));
        } else {
            return None;
        }
    }
}
