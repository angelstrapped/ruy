use rand::thread_rng;
use rand::seq::IteratorRandom;

use super::icosahedron;
use super::common::*;

use crate::objects::triangle::Triangle;
use crate::raytracer::vector3::Vector3;


fn r() -> f32 {
	return (rand::random::<f32>() -0.5) * 0.2;
}

fn i() -> f32 {
	return (rand::random::<f32>() -0.5) * 1.3;
}

fn j() -> f32 {
	return (rand::random::<f32>() + 3.5) * 0.5;
}

fn proportional_drag(mesh: &mut Vec<Vector3>, strength: f32, radius: f32) {
	let direction: Vector3 = Vector3::new(rand::random::<f32>() - 0.5, rand::random::<f32>() - 0.5, rand::random::<f32>() - 0.5).normalized();
	let distance: f32 = (rand::random::<f32>() - 0.5) * strength;
	let origin: Vector3 = mesh.iter().choose(&mut thread_rng()).unwrap().clone();

	for i in 0..mesh.iter().count() {
		if (mesh[i].x != origin.x) || (mesh[i].y != origin.y) || (mesh[i].z != origin.z) {
			let x: Vector3 = mesh[i];
			if (origin - x).magnitude() < radius {
				mesh[i] = x + (direction * (distance / (origin - x).magnitude()));
			}
		}
	}
}

fn smooth_foliage(mesh: &mut Vec<Vector3>, indices: &Vec<Vec<usize>>, strength: f32) {
	for x in indices.iter() {
		let prev: Vector3 = mesh[x[0]].clone();
		let next: Vector3 = mesh[x[1]].clone();
		let v: &mut Vector3 = &mut mesh[x[2]];

		v.x = smooth_generic(v.x, vec![prev.x, next.x], strength);
		v.y = smooth_generic(v.y, vec![prev.y, next.y], strength);
		v.z = smooth_generic(v.z, vec![prev.z, next.z], strength);

		let prev: Vector3 = mesh[x[1]].clone();
		let next: Vector3 = mesh[x[2]].clone();
		let v: &mut Vector3 = &mut mesh[x[0]];

		v.x = smooth_generic(v.x, vec![prev.x, next.x], strength);
		v.y = smooth_generic(v.y, vec![prev.y, next.y], strength);
		v.z = smooth_generic(v.z, vec![prev.z, next.z], strength);

		let prev: Vector3 = mesh[x[2]].clone();
		let next: Vector3 = mesh[x[0]].clone();
		let v: &mut Vector3 = &mut mesh[x[1]];

		v.x = smooth_generic(v.x, vec![prev.x, next.x], strength);
		v.y = smooth_generic(v.y, vec![prev.y, next.y], strength);
		v.z = smooth_generic(v.z, vec![prev.z, next.z], strength);
	}
}

fn make_foliage(tree_top: Vector3) -> Vec<Triangle> {
	let mut ico: icosahedron::Icosahedron = icosahedron::Icosahedron::big();
	let scale: Vector3 = Vector3::new(j(), j(), j());

	ico.vertices = ico.vertices.iter().map(|v| {
		let mut new_v: Vector3 = *v;

		// Scale.
		new_v = new_v * scale;

		// Jumble.
		new_v = new_v + Vector3::new(i(), i(), i());

		// Move.
		new_v = new_v + tree_top;

		return new_v;
	}).collect();

	proportional_drag(&mut ico.vertices, 1.5, 0.9);
	proportional_drag(&mut ico.vertices, 1.5, 0.9);
	proportional_drag(&mut ico.vertices, 1.5, 0.9);
	proportional_drag(&mut ico.vertices, 1.5, 0.9);
	proportional_drag(&mut ico.vertices, 1.5, 0.9);
	smooth_foliage(&mut ico.vertices, &ico.indices, 0.9);

	return ico.get_triangles();
}

fn make_tree(island_mesh: &Vec<Triangle>) -> Vec<Triangle> {
	let color: Vector3 = Vector3::new(0.1, 0.05, 0.03);
	let origin: Vector3 = island_mesh.iter().choose(&mut thread_rng()).unwrap().v0 - 2.0;
	let height: Vector3 = Vector3::new(
		rand::random::<f32>(),
		(rand::random::<f32>() * 5.0) + 2.0,
		rand::random::<f32>()
	);

	let s: f32 = r() * 2.0;

	let bottom: Vec<Vector3> = vec![
		Vector3::new(0.4 + s + r(), 0.0, 0.4 + s + (r() * 2.0)) + origin,
		Vector3::new(-0.4 + s + r(), 0.0, 0.4 + s + (r() * 2.0)) + origin,
		Vector3::new(-0.4 + s + r(), 0.0, -0.4 + s + (r() * 2.0)) + origin,
		Vector3::new(0.4 + s + r(), 0.0, -0.4 + s + (r() * 2.0)) + origin
	];

	let t: f32 = r() * 2.0;

	let top: Vec<Vector3> = vec![
		// Trunk base thickness 0.2 + random thickness t + random side offset r().
		Vector3::new(0.2 + t + r(), 0.0, 0.2 + t + (r() * 2.0)) + origin + height,
		Vector3::new(-0.2 + t + r(), 0.0, 0.2 + t + (r() * 2.0)) + origin + height,
		Vector3::new(-0.2 + t + r(), 0.0, -0.2 + t + (r() * 2.0)) + origin + height,
		Vector3::new(0.2 + t + r(), 0.0, -0.2 + t + (r() * 2.0)) + origin + height
	];

	let mut triangles: Vec<Triangle> = vec![
		Triangle::new(
			bottom[0],
			top[0],
			top[1],
			color
		),
		Triangle::new(
			bottom[0],
			top[1],
			bottom[1],
			color
		),
		Triangle::new(
			bottom[1],
			top[1],
			top[2],
			color
		),
		Triangle::new(
			bottom[1],
			top[2],
			bottom[2],
			color
		),
		Triangle::new(
			bottom[2],
			top[2],
			top[3],
			color
		),
		Triangle::new(
			bottom[2],
			top[3],
			bottom[3],
			color
		),
		Triangle::new(
			bottom[3],
			top[3],
			top[0],
			color
		),
		Triangle::new(
			bottom[3],
			top[0],
			bottom[0],
			color
		)
	];

	triangles.append(&mut make_foliage(origin + height));

	return triangles;
}

pub fn generate_trees(island_mesh: &Vec<Triangle>) -> Vec<Triangle>{
	let mut trees: Vec<Triangle> = Vec::new();
	trees.append(&mut make_tree(island_mesh));
	let mut num_trees: usize = 0;
	while rand::random::<f32>() > 0.4 {
		num_trees += 1;
		trees.append(&mut make_tree(island_mesh));
	}
	println!("Generated {} trees.", num_trees);
	return trees;
}
