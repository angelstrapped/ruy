use super::plane::generate_random_plane;
use super::tree;
use super::bird::Bird;

use crate::objects::camera::Camera;
use crate::objects::light::PointLight;
use crate::objects::scene::Scene;
use crate::objects::triangle::Triangle;

use crate::bounding::hierarchy::Hierarchy;
use crate::bounding::hierarchy::HierarchyChildren;

use crate::raytracer::vector3::Vector3;


pub struct Island;

impl Island {
	const NUM_VERTS: usize = 20;

    pub fn generate_random_light() -> PointLight {
        let random_origin: Vector3 = Vector3::new(
            rand::random::<f32>() - 0.5,
            rand::random::<f32>(),
            rand::random::<f32>() - 1.5
        ) * 10000.0;

        let random_brightness: f32 = rand::random::<f32>() + 1.5;

        return PointLight::new(
            random_origin,
            Vector3::new(0.0, 0.0, 0.0), // Direction.
            Vector3::new(0.9, 0.6, 0.2), // Color.
            random_brightness
        );
    }

    pub fn generate_scene() -> Scene {
        let water_plane: Triangle = Triangle::new(
            Vector3::new(0.0, 0.0, -900.0),
            Vector3::new(-900.0, 0.0, 500.0),
            Vector3::new(900.0, 0.0, 500.0),
            Vector3::new(0.0, 0.2, 0.5) // Color.
        );

        let camera: Camera = Camera::new(
            Vector3::new(
                10.0 + ((rand::random::<f32>() - 0.5) * 25.0),
                1.2,
                -25.0 +  ((rand::random::<f32>() - 0.5) * 5.0)
            ), // Origin.
            Vector3::new(
                0.0,
                0.1 + ((rand::random::<f32>() - 0.5) * 0.2),
                1.0
            ), // Direction.
            1.5 // Focal length.
        );

		let mut island: Vec<Triangle> = generate_random_plane(Island::NUM_VERTS);
		let mut trees: Vec<Triangle> = tree::generate_trees(&island);

		island.append(&mut trees);

		println!("Number of triangles: {:?}", island.iter().count());

        let lights: Vec<PointLight> = vec![Island::generate_random_light()];

		let mut hierarchy = Hierarchy::new(&mut island);

		let mut water_hierarchy: Hierarchy = Hierarchy::new(&mut vec!(water_plane));

		let mut bird: Hierarchy = Hierarchy::new(&mut Bird::generate_birds());

		let objects: Vec<Hierarchy> = vec![hierarchy, water_hierarchy, bird];


        return Scene::new(objects, lights, camera);
    }
}
