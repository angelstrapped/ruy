use crate::objects::triangle::Triangle;

pub fn smooth_generic(value: f32, surrounding_values: Vec<f32>, strength: f32) -> f32 {
	let weakness: f32 = 1.0 / strength;
	return (surrounding_values.iter().sum::<f32>() + (value * weakness)) / (surrounding_values.iter().count() as f32 + weakness);
}

// For removing all the tris below the water plane.
// Since we haven't implemented any sort of BVH or such, yet,
// we want as few tris as possible.
pub fn triangle_is_below(triangle: Triangle, threshold: f32) -> bool {
	for v in vec![triangle.v0, triangle.v1, triangle.v2].iter() {
		if v.y > threshold {
			return false;
		}
	}
	return true;
}
