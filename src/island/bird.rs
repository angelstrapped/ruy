use std::path::Path;

use crate::drivers::dae::DAE;
use crate::objects::scene::Scene;
use crate::objects::triangle::Triangle;
use crate::raytracer::vector3::Vector3;

pub struct Bird;

impl Bird {
	pub fn generate_bird() -> Vec<Triangle> {
		let bird_triangles = vec![
		Triangle::new(Vector3::new(-1.414214, 0.0, 0.0), Vector3::new(0.0, 1.414214, 0.0), Vector3::new(0.0, 0.0, 4.0 ), Vector3::new(0.6038158, 0.6038158, 0.6038158 )),
		Triangle::new(Vector3::new(0.0, -1.414214, 0.0), Vector3::new(1.414214, 0.0, 0.0), Vector3::new(0.0, 0.0, -4.0 ), Vector3::new(0.6038158, 0.6038158, 0.6038158 )),
		Triangle::new(Vector3::new(-1.414214, 0.0, 0.0), Vector3::new(0.0, -1.414214, 0.0), Vector3::new(0.0, 0.0, -4.0 ), Vector3::new(0.6038158, 0.6038158, 0.6038158 )),
		Triangle::new(Vector3::new(1.414214, 0.0, 0.0), Vector3::new(0.0, 1.414214, 0.0), Vector3::new(0.0, 0.0, -4.0 ), Vector3::new(0.6038158, 0.6038158, 0.6038158 )),
		Triangle::new(Vector3::new(0.0, 0.0, 4.0), Vector3::new(0.0, 1.414214, 0.0), Vector3::new(1.414214, 0.0, 0.0 ), Vector3::new(0.6038158, 0.6038158, 0.6038158 )),
		Triangle::new(Vector3::new(0.0, 0.0, 4.0), Vector3::new(0.0, -1.414214, 0.0), Vector3::new(-1.414214, 0.0, 0.0 ), Vector3::new(0.6038158, 0.6038158, 0.6038158 )),
		Triangle::new(Vector3::new(0.0, 0.0, 4.0), Vector3::new(1.414214, 0.0, 0.0), Vector3::new(0.0, -1.414214, 0.0 ), Vector3::new(0.6038158, 0.6038158, 0.6038158 )),
		Triangle::new(Vector3::new(0.0, 0.0, -4.0), Vector3::new(0.0, 1.414214, 0.0), Vector3::new(-1.414214, 0.0, 0.0 ), Vector3::new(0.6038158, 0.6038158, 0.6038158 )),
		Triangle::new(Vector3::new(6.88, 3.32, -2.56), Vector3::new(0.4799998, 0.1199998, 2.24), Vector3::new(0.4799998, 0.1199998, -2.24 ), Vector3::new(0.6038158, 0.6038158, 0.6038158 )),
		Triangle::new(Vector3::new(6.88, 3.32, 2.56), Vector3::new(6.88, 3.32, -2.56), Vector3::new(13.28, 0.1199998, 0.0 ), Vector3::new(0.6038158, 0.6038158, 0.6038158 )),
		Triangle::new(Vector3::new(-6.88, 3.32, 2.56), Vector3::new(-0.4799996, 0.1199998, -2.24), Vector3::new(-0.48, 0.1199998, 2.24 ), Vector3::new(0.6038158, 0.6038158, 0.6038158 )),
		Triangle::new(Vector3::new(-6.88, 3.32, -2.560001), Vector3::new(-6.88, 3.32, 2.56), Vector3::new(-13.28, 0.1199998, -0.00000117528 ), Vector3::new(0.6038158, 0.6038158, 0.6038158 )),
		Triangle::new(Vector3::new(6.88, 3.32, -2.56), Vector3::new(6.88, 3.32, 2.56), Vector3::new(0.4799998, 0.1199998, 2.24 ), Vector3::new(0.6038158, 0.6038158, 0.6038158 )),
		Triangle::new(Vector3::new(-6.88, 3.32, 2.56), Vector3::new(-6.88, 3.32, -2.560001), Vector3::new(-0.4799996, 0.1199998, -2.24 ), Vector3::new(0.6038158, 0.6038158, 0.6038158 ))];

		return bird_triangles;
	}

	pub fn translate_triangles(triangles: Vec<Triangle>, distance: Vector3) -> Vec<Triangle> {
		return triangles.iter().map(|t| {
			Triangle::new(
				t.v0 + distance,
				t.v1 + distance,
				t.v2 + distance,
				t.color
			)
		}).collect();
	}

	pub fn scale_triangles(triangles: Vec<Triangle>, factor: f32) -> Vec<Triangle> {
		return triangles.iter().map(|t| {
			Triangle::new(
				t.v0 * factor,
				t.v1 * factor,
				t.v2 * factor,
				t.color
			)
		}).collect();
	}

	pub fn rotate_triangles(triangles: Vec<Triangle>, angle: f32) -> Vec<Triangle> {
		return triangles.iter().map(|t| {
			Triangle::new(
				Vector3::new(
					t.v0.x * angle.cos() - t.v0.z * angle.sin(),
					t.v0.y,
					t.v0.z * angle.cos() + t.v0.x * angle.sin()
				),
				Vector3::new(
					t.v1.x * angle.cos() - t.v1.z * angle.sin(),
					t.v1.y,
					t.v1.z * angle.cos() + t.v1.x * angle.sin()
				),
				Vector3::new(
					t.v2.x * angle.cos() - t.v2.z * angle.sin(),
					t.v2.y,
					t.v2.z * angle.cos() + t.v2.x * angle.sin()
				),
				t.color
			)
		}).collect();
	}

	pub fn generate_rotated_bird() ->Vec<Triangle> {
		// Height between 20.0 and 30.0.
		let height: f32 = (rand::random::<f32>() * 10.0) + 20.0;

		// Direction betweeon 0.0 and 360.0.
		let direction: f32 = (rand::random::<f32>() * 360.0);

		// X position between -100.0 and 100.0.
		let x: f32 = (rand::random::<f32>() - 0.5) * 200.0;

		// Z position between -100.0 and 100.0.
		let z: f32 = (rand::random::<f32>() - 0.5) * 200.0;

		let bird: Vec<Triangle> = Bird::generate_bird();

		return Bird::translate_triangles(
			Bird::scale_triangles(
				Bird::rotate_triangles(
					bird,
					direction
				),
				0.2
			),
			Vector3::new(x, height, z)
		);
	}

	pub fn generate_birds() -> Vec<Triangle>{
		let mut birds: Vec<Triangle> = Vec::new();
		let mut num_birds: usize = 0;
		while rand::random::<f32>() > 0.4 {
			num_birds += 1;
			birds.append(&mut Bird::generate_rotated_bird());
		}
		println!("Generated {} birds.", num_birds);
		return birds;
	}
}
