use crate::objects::triangle::Triangle;
use crate::raytracer::vector3::Vector3;

use super::common::*;

fn smooth_plane_height(mesh: &mut Vec<Vec<Vector3>>, strength: f32) {
	for y in 1..mesh.iter().count() - 1 {
		for x in 1..mesh[0].iter().count() - 1 {
			let prev_x: Vector3 = mesh[y][x - 1].clone();
			let next_x: Vector3 = mesh[y][x + 1].clone();
			let prev_z: Vector3 = mesh[y - 1][x].clone();
			let next_z: Vector3 = mesh[y + 1][x].clone();
			let v: &mut Vector3 = &mut mesh[y][x];

			v.y = smooth_generic(v.y, vec![prev_x.y, next_x.y, prev_z.y, next_z.y], strength);
		}
	}
}

pub fn generate_random_plane(num_verts: usize) -> Vec<Triangle> {
	let color: Vector3 = Vector3::new(0.3, 0.2, 0.05);

	// We need a 2D array of VERTICES.
	let mut vertex_array: Vec<Vec<Vector3>> = vec![
		vec![
			Vector3::new(0.0, 0.0, 0.0);
			num_verts
		];
		num_verts
	];

	// Move the vertices up and down.
	// According to my notes, sin(90) is the highest sine value,
	// and sin(180) is back to 0.0.
	// So we want the last vertex to be n * (NUM_VERTS / 180) or something.
	let multiplier: f32 = (num_verts as f32 - 1.0) / 180.0;
	for y in 1..num_verts {
		for x in 1..num_verts {
			let v: &mut Vector3 = &mut vertex_array[y][x];

			v.x = x as f32;
			v.z = y as f32;
		}
	}

	for y in 1..num_verts - 1 {
		for x in 1..num_verts - 1 {
			let v: &mut Vector3 = &mut vertex_array[y][x];

			let x_sin: f32 = (v.x / multiplier as f32).to_radians().sin();
			let y_sin: f32 = (v.z / multiplier as f32).to_radians().sin();

			v.y = (((rand::random::<f32>() - 0.5) * 9.0) + ((x_sin + y_sin) * (5.0 + rand::random::<f32>()))) - 9.0;
		}
	}

	smooth_plane_height(&mut vertex_array, 1.0);
	smooth_plane_height(&mut vertex_array, 1.0);
	smooth_plane_height(&mut vertex_array, 1.0);

	// Then wind the triangles.
	// Basically build triangles up to the second-to-last column and row,
	// and it will work.
	// if x < NUM_VERTS - 1 and y < NUM_VERTS - 1:
	let mut triangles: Vec<Triangle> = vec![];

	let mut tri: Triangle;
	for y in 0..num_verts - 1 {
		for x in 0..num_verts - 1 {
			// Top left, top right, bottom left.
			tri = Triangle::new(
				vertex_array[y][x],
				vertex_array[y][x + 1],
				vertex_array[y + 1][x],
				color
			);

			if !triangle_is_below(tri, 0.0) {
				triangles.push(tri);
			}

			// Top right, bottom right, bottom left.
			tri = Triangle::new(
				vertex_array[y][x + 1],
				vertex_array[y + 1][x + 1],
				vertex_array[y + 1][x],
				color
			);

			if !triangle_is_below(tri, 0.0) {
				triangles.push(tri);
			}
		}
	}

	return triangles;
}
