use rand::prelude::*;

use std::time::Instant;

use super::vector3::Vector3;
use super::ray::Ray;

use crate::drivers::image::Image;

use crate::objects::camera::Camera;
use crate::objects::scene::Scene;
use crate::objects::triangle::Triangle;
use crate::objects::light::PointLight;

const FILM_RADIUS: f32 = 1.0;

struct Hit {
	distance: f32,
	mesh: Triangle
}

impl Hit {
	fn new(distance: f32, mesh: Triangle) -> Hit {
		return Hit {
			distance: distance,
			mesh: mesh
		}
	}
}

pub struct Raytracer {
	scene: Scene
}

impl Raytracer {
	pub fn new(scene: Scene) -> Raytracer {
		return Raytracer {
			scene: scene
		}
	}

	fn diffuse_bounce_direction(&self, normal: Vector3) -> Vector3 {
		// Sort of like the sphere thing from RTIOW.
		let p = (Vector3::new(random(), random(), random()) * 2.0) - Vector3::new(1.0, 1.0, 1.0);
		return (p + normal).normalized();
	}

	fn in_shadow(&self, point: Vector3, light: &PointLight) -> bool {
		let ray_direction: Vector3 = (light.origin - point).normalized();
		let ray: Ray = Ray::new(point, ray_direction);

		let trace: Option<Hit> = self.find_nearest_mesh(ray);
		match trace {
			Some(_) => {
				return true;
			},
			None => {
				return false;
			}
		}
	}

	fn color_at(&self, mesh: Triangle, point: Vector3) -> Vector3 {
		let mut color = Vector3::new(0.0, 0.0, 0.0);

		// "Ambient color".
		color = color + mesh.color * 0.75;

		for light in &self.scene.lights {
			let shadow: bool = self.in_shadow(point, light);
			if !shadow {
				let dot: f32 = (light.origin - point).normalized().dot(mesh.normal().normalized());

				// Add about 10% of the light's intrinsic color.
				let blended_color: Vector3 = (mesh.color + (light.color * 0.1)) * 0.9;
				color = color + (blended_color * (light.brightness / (1.0 / dot.abs())));
			}
		}
		return color;
	}

	fn find_nearest_mesh(&self, ray: Ray) -> Option<Hit> {
		let mut hit_mesh: Option<&Triangle> = None;
		let mut hit_distance: Option<f32> = None;

		for mesh in &self.scene.objects {
			let hit: Option<(f32, &Triangle)> = mesh.intersect(ray);

			match hit {
				Some(d) => {
					match hit_distance {
						Some(h) => {
							if d.0 < h {
								hit_distance = Some(d.0);
								hit_mesh = Some(d.1);
							}
						},
						None => {
							hit_distance = Some(d.0);
							hit_mesh = Some(d.1);
						}
					}
				},
				None => ()
			}
		}

		match hit_mesh {
			Some(t) => return Some(Hit::new(
				hit_distance.unwrap(),
				*t
			)),
			None => return None
		}
	}

	fn diffuse_sample(&self, hit: &Hit, intersection_point: Vector3) -> Vector3 {
		let mut color: Vector3 = Vector3::new(0.0, 0.0, 0.0);
		let diffuse_ray: Ray = Ray::new(intersection_point, self.diffuse_bounce_direction(hit.mesh.n));

		let diffuse_trace: Option<Hit> = self.find_nearest_mesh(diffuse_ray);
		match diffuse_trace {
			Some(h) => {
				if h.distance < 3.0 {
					color = color + h.mesh.color;
				}
			},
			None => ()
		}

		return color;
	}

	fn sky(&self, ray: Ray, sky_color: Vector3) -> Vector3 {
		//println!("{:?} {:?} {:?}", y, sky_color, (sky_color * ((y + 0.4) * 5.0)));
		return (sky_color * (1.0 / ((ray.direction.y * 6.0) + 0.5)) + Vector3::new(1.0, 1.0, 1.0)) / 2.0;
	}

	fn raytrace(&self, ray: Ray, sky_color: Vector3, num_diffuse_samples: usize) -> Vector3 {
		let mut color: Vector3;

		let hit: Hit;
		let trace: Option<Hit> = self.find_nearest_mesh(ray);
		match trace {
			Some(h) => {
				hit = h;
			},
			None => {
				return self.sky(ray, sky_color);
			}
		}

		let intersection_point = ray.origin + (ray.direction * hit.distance);
		color = self.color_at(hit.mesh, intersection_point);

		for i in 0..num_diffuse_samples {
			color = color + (self.diffuse_sample(&hit, intersection_point) / num_diffuse_samples as f32);
		}

		return color;
	}

	pub fn render_random(&self, image: &mut Image, pass_multiplier: f32, samples: usize, num_diffuse_samples: usize) {
		// Random sampling.
		println!("Random pass at {} strength.", pass_multiplier);

		let start = Instant::now();

		let aspect_ratio: f32 = image.width as f32 / image.height as f32;

		let x_min: f32 = -FILM_RADIUS;
		let x_max: f32 = FILM_RADIUS;

		let y_min: f32 = -FILM_RADIUS / aspect_ratio;
		let y_max: f32 = FILM_RADIUS / aspect_ratio;

		let x_step: f32 = (x_max - x_min) / (image.width - 1) as f32;
		let y_step: f32 = (y_max - y_min) / (image.height - 1) as f32;

		let camera: Camera = self.scene.active_camera;

		let mut rng = thread_rng();

		let sky_color = Vector3::new(rand::random(), 0.0, rand::random());

		let total_samples: usize = image.height * image.width * samples;
		for i in 0..total_samples {
			if i % 100000 == 0 {
				println!("Sample {} of {}.", i, total_samples);
			}

			let col: usize = rng.gen_range(0, image.width);
			let row: usize = rng.gen_range(0, image.height);

			let y: f32 = y_min + (y_step * row as f32);
			let x: f32 = x_min + (x_step * col as f32);

			let ray_origin: Vector3 = camera.origin;
			// I think this -x thing might be a symptom of an issue with interpreting the input scene.
			// We'll find out once the camera is implemented.
			let pixel_origin: Vector3 = (camera.origin + (camera.direction * camera.focal_length)) - Vector3::new(-x, y, 0.0);
			let ray_direction: Vector3 = (pixel_origin - ray_origin).normalized();

			let ray: Ray = Ray::new(ray_origin, ray_direction);
			let color: Vector3 = self.raytrace(ray, sky_color, num_diffuse_samples) / samples as f32;
			image.set_pixel(col, row, image.get_pixel(col, row) + (color * pass_multiplier));
		}
		println!("Random pass took {}s", start.elapsed().as_secs());
	}

	pub fn render(&self, image: &mut Image, pass_multiplier: f32, samples: usize, num_diffuse_samples: usize) {
		// Linear sampling.
		println!("Linear pass at {} strength.", pass_multiplier);
		let start = Instant::now();

		let aspect_ratio: f32 = image.width as f32 / image.height as f32;

		let x_min: f32 = -FILM_RADIUS;
		let x_max: f32 = FILM_RADIUS;

		let y_min: f32 = -FILM_RADIUS / aspect_ratio;
		let y_max: f32 = FILM_RADIUS / aspect_ratio;

		let x_step: f32 = (x_max - x_min) / (image.width - 1) as f32;
		let y_step: f32 = (y_max - y_min) / (image.height - 1) as f32;

		let camera: Camera = self.scene.active_camera;

		let sky_color = Vector3::new(rand::random::<f32>(), rand::random::<f32>() / 2.0, rand::random::<f32>());

		for row in 0..image.height {
			if row % 100 == 0 {
				println!("Row {} of {}.", row, image.height);
			}

			let y: f32 = y_min + (y_step * row as f32);
			for col in 0..image.width {
				let x: f32 = x_min + (x_step * col as f32);

				// TODO: Rotate the pixel_offset by the camera's rotation matrix.
				// Otherwise only cameras pointing along the Z axis are correct.
				let pixel_offset: Vector3 = Vector3::new(-x, y, 0.0);
				let ray_origin: Vector3 = camera.origin;
				let pixel_origin: Vector3 = (camera.origin + (camera.direction * camera.focal_length)) - pixel_offset;
				let ray_direction: Vector3 = (pixel_origin - ray_origin).normalized();

				let ray: Ray = Ray::new(ray_origin, ray_direction);
				let color: Vector3 = self.raytrace(ray, sky_color, num_diffuse_samples);
				image.set_pixel(col, row, color * pass_multiplier);
			}
		}
		println!("Linear pass took {}s", start.elapsed().as_secs());
	}
}
