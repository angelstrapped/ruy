use std::ops::{Add, Sub, Mul, Div};

#[derive(Copy, Clone, Debug)]
pub struct Vector3 {
    pub x: f32,
    pub y: f32,
    pub z: f32
}

impl Vector3 {
    pub fn new(x: f32, y: f32, z: f32) -> Vector3 {
        return Vector3 {
            x: x,
            y: y,
            z: z
        }
    }

    pub fn magnitude(&self) -> f32 {
        return self.dot(*self).sqrt();
    }

    pub fn dot(&self, other: Vector3) -> f32 {
        return self.x * other.x + self.y * other.y + self.z * other.z;
    }

    pub fn cross(&self, other: Vector3) -> Vector3 {
        return Vector3::new(
            (self.y * other.z) - (self.z * other.y),
            (self.z * other.x) - (self.x * other.z),
            (self.x * other.y) - (self.y * other.x)
        );
    }

    pub fn normalized(&self) -> Vector3 {
        let magnitude: f32 = self.magnitude();

        return Vector3::new(
            self.x / magnitude,
            self.y / magnitude,
            self.z / magnitude
        );
    }
}

impl Sub<Self> for Vector3 {
    type Output = Self;

    fn sub(self, other: Self) -> Self{
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z
        }
    }
}

impl Sub<f32> for Vector3 {
    // I guess this one isn't very useful.
    type Output = Self;

    fn sub(self, other: f32) -> Self{
        Self {
            x: self.x - other,
            y: self.y - other,
            z: self.z - other
        }
    }
}

impl Add<Self> for Vector3 {
    type Output = Self;

    fn add(self, other: Self) -> Self{
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z
        }
    }
}

impl Add<f32> for Vector3 {
    // I guess this one isn't very useful.
    type Output = Self;

    fn add(self, other: f32) -> Self{
        Self {
            x: self.x + other,
            y: self.y + other,
            z: self.z + other
        }
    }
}

impl Mul<f32> for Vector3 {
    type Output = Self;

    fn mul(self, other: f32) -> Self{
        Self {
            x: self.x * other,
            y: self.y * other,
            z: self.z * other
        }
    }
}

impl Mul<Self> for Vector3 {
    type Output = Self;

    fn mul(self, other: Self) -> Self{
        Self {
            x: self.x * other.x,
            y: self.y * other.y,
            z: self.z * other.z
        }
    }
}

impl Div<f32> for Vector3 {
    type Output = Self;

    fn div(self, other: f32) -> Self{
        Self {
            x: self.x / other,
            y: self.y / other,
            z: self.z / other
        }
    }
}

impl Div<Self> for Vector3 {
    type Output = Self;

    fn div(self, other: Self) -> Self{
        Self {
            x: self.x / other.x,
            y: self.y / other.y,
            z: self.z / other.z
        }
    }
}
