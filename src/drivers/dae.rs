extern crate collada;
extern crate xml;

use std::path::Path;
use std::collections::HashMap;

use collada::document::ColladaDocument;
use collada::ObjSet;
use collada::Geometry;
use collada::Triangles;
use collada::Vertex;
use collada::VertexIndex;
use collada::document::LambertEffect;

use crate::objects::scene::Scene;
use crate::objects::camera::Camera;
use crate::objects::light::PointLight;
use crate::objects::triangle::Triangle;

use crate::raytracer::vector3::Vector3;

use crate::bounding::hierarchy::Hierarchy;

pub struct DAE;

impl DAE {
    fn direction_from_matrix(matrix: Vec<f32>, initial: Vector3) -> Vector3 {
        return Vector3::new(
            (matrix[0] * initial.x) + (matrix[1] * initial.y) + (matrix[2] * initial.z),
			(matrix[4] * initial.x) + (matrix[5] * initial.y) + (matrix[6] * initial.z),
			(matrix[8] * initial.x) + (matrix[9] * initial.y) + (matrix[10] * initial.z)
        );
    }

    fn get_camera(root: &xml::Element, library_visual_scenes: &xml::Element) -> Camera {
        let xmlns: Option<&str> = Some("http://www.collada.org/2005/11/COLLADASchema");

        let cameras: Vec<&xml::Element> = root.get_child("library_cameras", xmlns)
            .expect("No camera library.")
            .get_children("camera", xmlns)
            .collect::<Vec<&xml::Element>>();

        if cameras.len() > 0 {
            let name: &str = cameras[0]
                .get_attribute("name", None)
                .expect("Camera has no name.");

            let camera: &xml::Element = cameras[0].get_child("optics", xmlns)
                .expect("No optics.")
                .get_child("technique_common", xmlns)
                .expect("No technique.")
                .get_child("perspective", xmlns)
                .expect("No perspective.");

            let focal_length: f32 = camera.get_child("xfov", xmlns)
                .expect("No horizontal FOV.")
                .content_str()
                .parse::<f32>()
                .expect("Could not parse float in FOV.")
                .atan();

            let matrix: Vec<f32> = library_visual_scenes
                /**/
                // Put this part in get_node_from_visual().

                // There might be multiple of these, I guess, but flhfwph...
                .get_child("visual_scene", xmlns)
                .expect("No visual_scene.")
                .get_children("node", xmlns)
                .filter(|n|
                    n.get_attribute("name", None).expect("Node has no name.") == name
                ).collect::<Vec<&xml::Element>>()[0]
                /**/

                /**/
                // This part could be get_matrix_from_node().
                .get_child("matrix", xmlns)
                .expect("Node has no matrix.")
                .content_str()
                .split(" ")
                .map(|s|
                    s.parse::<f32>()
                    .expect("Could not parse float in matrix.")
                ).collect();
                /**/

            let coordinates: Vector3 = Vector3::new(matrix[3], matrix[7], matrix[11]);

            // It seems the camera defaults to pointing straight down,
            // so we apply the camera's rotation matrix to a down vector.
            let initial: Vector3 = Vector3::new(0.0, -1.0, 0.0);
            let direction: Vector3 = DAE::direction_from_matrix(matrix, initial);

            // TODO: Add the rotation matrix to the Camera struct.
            return Camera::new(
                coordinates,
                direction,
                focal_length
            );
        } else {
            // Spit out a default camera if none exists in the library.
            return Camera::new(
                Vector3::new(0.0, 0.0, 6.0),
                Vector3::new(0.0, 0.0, -1.0),
                1.35
            );
        }
    }

    fn light_from_xml(
        element: &xml::Element,
        library_visual_scenes: &xml::Element,
        xmlns: Option<&str>
    ) -> PointLight {
        // Suddenly at this level, namespace is None.
        let name: &str = element
        .get_attribute("name", None)
        .expect("Light has no name.");

        let matrix: Vec<f32> = library_visual_scenes
            // There might be multiple of these, I guess, but flhfwph...
            .get_child("visual_scene", xmlns)
            .expect("No visual_scene.")
            .get_children("node", xmlns)
            .filter(|n|
                n.get_attribute("name", None).expect("Node has no name.") == name
            ).collect::<Vec<&xml::Element>>()[0]
            .get_child("matrix", xmlns)
            .expect("Node has no matrix.")
            .content_str()
            .split(" ")
            .map(|s|
                s.parse::<f32>()
                .expect("Could not parse float in matrix.")
            ).collect();

        let coordinates: Vector3 = Vector3::new(matrix[3], matrix[7], matrix[11]);

        let energy: f32 = element
            .get_child("extra", xmlns)
            .expect("No property 'extra' in light.")
            .get_child("technique", xmlns)
            .expect("No property 'technique' in light.")
            .get_child("energy", xmlns)
            .expect("No property 'energy' in light.")
            .content_str()
            .parse::<f32>()
            .expect("Could not parse energy to float.");

        let technique: Option<&xml::Element> = element.get_child("technique_common", xmlns);

        let point: Option<&xml::Element> = match technique {
            Some(e) => e.get_child("point", xmlns),
            None => None
        };

        let color_element: Option<&xml::Element> = match point {
            Some(e) => e.get_child("color", xmlns),
            None => None
        };

        let color_split: Option<Vec<f32>> = match color_element {
            Some(e) => Some(e.content_str().split_whitespace().map(|s|
                s.parse::<f32>().unwrap()
            ).collect()),
            None => None
        };

        let color: Vector3 = match color_split {
            Some(s) => Vector3::new(s[0] / energy, s[1] / energy, s[2] / energy),
            None => Vector3::new(1.0, 1.0, 1.0)
        };

        return PointLight::new(
            coordinates,
            // PointLight direction alkfdjnbskjgbfdjn.
            // I'll get around to doing that matrix transform stuff, eventually.
            Vector3::new(0.0, 0.0, 0.0),
            color,
            energy
        );
    }

    fn get_lights(root: &xml::Element) -> Vec<PointLight> {
        let xmlns: Option<&str> = Some("http://www.collada.org/2005/11/COLLADASchema");

        let library_visual_scenes: &xml::Element = root.get_child(
            "library_visual_scenes",
            xmlns
        ).expect("No library_visual_scenes.");

        let library_lights: &xml::Element = root.get_child(
            "library_lights",
            xmlns
        ).expect("No lights element.");

        let light_elements: xml::ChildElements = library_lights.get_children(
            "light",
            xmlns
        );

        let lights: Vec<PointLight> = light_elements.map(|e|
            DAE::light_from_xml(e, library_visual_scenes, xmlns)
        ).collect();

        return lights;
    }

    fn triangle_from_vertex_data(
        indices: (VertexIndex, VertexIndex, VertexIndex),
        vertices: &Vec<Vertex>,
        material: Option<&LambertEffect>
    ) -> Triangle {
        println!("{:?}", indices);
        let v0: Vertex = vertices[indices.0];
        let v1: Vertex = vertices[indices.1];
        let v2: Vertex = vertices[indices.2];

        let color: Vector3 = match material {
            Some(m) => Vector3::new(m.diffuse[0], m.diffuse[1], m.diffuse[2]),
            None => Vector3::new(1.0, 1.0, 1.0)
        };

        return Triangle::new(
            Vector3::new(v0.x as f32, v0.y as f32, v0.z as f32),
            Vector3::new(v1.x as f32, v1.y as f32, v1.z as f32),
            Vector3::new(v2.x as f32, v2.y as f32, v2.z as f32),
            color
        );
    }

    fn triangles_from_object(
        object: &collada::Object,
        effect_library: &HashMap<String, LambertEffect>,
        material_to_effect: &HashMap<String, String>
    ) -> Vec<Triangle> {
        let vertices: &Vec<Vertex> = &object.vertices;
        let geometry: &Vec<Geometry> = &object.geometry;

        let primitives: Vec<&collada::PrimitiveElement> = geometry.iter().map(|g|
            &g.mesh
        ).flatten().collect();

        let triangle_primitives: Vec<&Triangles> = primitives.iter().cloned().filter_map(|p|
            // Triangles is the PrimitiveElement variant, t is a DIFFERENT Triangles.
            if let collada::PrimitiveElement::Triangles(t) = p {
                return Some(t);
            } else {
                return None;
            }
        ).collect();

        return triangle_primitives.iter().map(|t|
            t.vertices.iter().cloned().map(move |v|
                return DAE::triangle_from_vertex_data(
                    v,
                    &vertices,
                    match &t.material {
                        Some(m) => {
                            effect_library.get(material_to_effect.get(m).expect("No effect."))
                        },
                        None => None
                    }
                )
            )
        ).flatten().collect::<Vec<Triangle>>();
    }

    fn build_scene(data: ColladaDocument) -> Scene {
        let xmlns: Option<&str> = Some("http://www.collada.org/2005/11/COLLADASchema");
        let root: &xml::Element = &data.root_element;

        let object_set: Option<ObjSet> = data.get_obj_set();
        let effect_library: HashMap<String, LambertEffect> = data.get_effect_library();
        let material_to_effect: HashMap<String, String> = data.get_material_to_effect();
        let library_visual_scenes: &xml::Element = root.get_child(
            "library_visual_scenes",
            xmlns
        ).expect("No library_visual_scenes.");

        let camera: Camera = DAE::get_camera(root, library_visual_scenes);

        let lights: Vec<PointLight> = DAE::get_lights(root);

        let mut triangles: Vec<Triangle> = object_set.unwrap().objects.iter().map(|x|
                DAE::triangles_from_object(x, &effect_library, &material_to_effect)
            ).flatten().collect();

        return Scene::new(vec![Hierarchy::new(&mut triangles)], lights, camera);
    }

	fn build_triangles(data: ColladaDocument) -> Vec<Triangle> {
        let xmlns: Option<&str> = Some("http://www.collada.org/2005/11/COLLADASchema");
        let root: &xml::Element = &data.root_element;

        let object_set: Option<ObjSet> = data.get_obj_set();
        let effect_library: HashMap<String, LambertEffect> = data.get_effect_library();
        let material_to_effect: HashMap<String, String> = data.get_material_to_effect();
        let library_visual_scenes: &xml::Element = root.get_child(
            "library_visual_scenes",
            xmlns
        ).expect("No library_visual_scenes.");

        let mut triangles: Vec<Triangle> = object_set.unwrap().objects.iter().map(|x|
                DAE::triangles_from_object(x, &effect_library, &material_to_effect)
            ).flatten().collect();

        return triangles;
    }

    pub fn load_scene(path: &Path) -> Scene {
        let dae: Result<ColladaDocument, &str> = ColladaDocument::from_path(path);

        let data: ColladaDocument = dae.unwrap();
        return DAE::build_scene(data);
    }

	pub fn load_triangles(path: &Path) -> Vec<Triangle> {
        let dae: Result<ColladaDocument, &str> = ColladaDocument::from_path(path);

        let data: ColladaDocument = dae.unwrap();
        return DAE::build_triangles(data);
    }
}
