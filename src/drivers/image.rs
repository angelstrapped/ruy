extern crate image;

use std::cmp;
use std::path::Path;

use crate::raytracer::vector3::Vector3;

#[derive(Clone, Debug)]
pub struct Image {
    pub width: usize,
    pub height: usize,
    color_depth: u32,
    color_max: u32,
    // I've yet to figure out what to do about Color as a subclass of Vector3,
    // so we're using regular Vector3s as color, for now.
    pixels: Vec<Vec<Vector3>>
}

impl Image {
    pub fn new(width: usize, height: usize, color_depth: u32, bg_color: Vector3) -> Image {
        return Image {
            width: width,
            height: height,
            color_depth: color_depth,
            color_max: color_depth - 1,
            pixels: vec![vec![bg_color; width]; height]
        }
    }

    pub fn set_pixel(&mut self, col: usize, row: usize, color: Vector3) {
        self.pixels[row][col] = color;
    }

    pub fn get_pixel(&self, col: usize, row: usize) -> Vector3 {
        return self.pixels[row][col];
    }

    fn to_depth(&self, float: f32) -> u32 {
        // Convert from for e.g. "0.5" to "127".
        let max_float: f32 = self.color_max as f32;
        let scaled_value: u32 = (float * max_float) as u32;
        return cmp::max(cmp::min(scaled_value, self.color_max), 0);
    }

    pub fn save_jpg(&self, path: &Path) {
        let mut buffer = image::ImageBuffer::new(self.width as u32, self.height as u32);
        for y in 0..self.height {
            for x in 0..self.width {
                *buffer.get_pixel_mut(x as u32, y as u32) = image::Rgb([
                    self.to_depth(self.pixels[y][x].x) as u8,
                    self.to_depth(self.pixels[y][x].y) as u8,
                    self.to_depth(self.pixels[y][x].z) as u8
                ]);
            }
        }
        buffer.save(path).unwrap();
    }

    pub fn output(&self) -> String {
        // Spit the image out as a string in PPM format.
        // I feel like Image should not be responsible for handling the file descriptor.
        let header: String = format!("P3 {} {}\n{}\n", self.width, self.height, self.color_depth);

        let mapped_pixels: Vec<Vec<String>> = self.pixels.iter().map(
            |y| y.iter().map(
                |x| format!("{} {} {}", self.to_depth(x.x), self.to_depth(x.y), self.to_depth(x.z))
            ).collect()
        ).collect();

        return format!(
            // This space at the end of the PPM seems to be quite important.
            "{}{} ",
            header,
            mapped_pixels.iter().map(
                |x| x.join(" ")
            ).collect::<Vec<String>>().join("\n")
        );
    }
}
