// This was my initial idea.
pub fn below_threshold(triangle: Triangle, threshold: f32) -> bool {
	vec![triangle.v0, triangle.v1, triangle.v2]
		.iter()
		.for_each(|v|
			if v.y > threshold {
				return false;
			}
		);
		// Error: Mismatched types.
		// Surely for_each() is void. How could there be a type error here?
		// I guess Rust wants the vec![] expression to be return from below_threshold().
	return true;
}

// This is what I wanted to accomplish.
pub fn triangle_is_below(triangle: Triangle, threshold: f32) -> bool {
	for v in vec![triangle.v0, triangle.v1, triangle.v2].iter() {
		if v.y > threshold {
			return false;
		}
	}
	return true;
}
